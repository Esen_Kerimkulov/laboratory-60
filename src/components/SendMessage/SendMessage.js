import React from 'react';
import './SendMessage.css';

const SendMessage = (props) => {
    return (
        <div>
            <input type="text" onChange={props.onChangeText} value={props.text} placeholder="Message"/>
            <input type="text" onChange={props.onChangeAuthor} value={props.author} placeholder="Author"/>
            <button className="btn" type="button" onClick={props.addMessage} >Send</button>
        </div>
    );
};

export default SendMessage;