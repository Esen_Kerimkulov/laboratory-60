import React, { Component } from 'react';
import GetMessage from '../GetMessage/GetMessage';
import SendMessage from '../SendMessage/SendMessage';

class Message extends Component {
    state = {
        messages: [],
        author: '',
        text: ''
    };

    componentDidMount() {
        fetch('http://146.185.154.90:8000/messages' ).then(response => {
            if (response.ok) {
                return response.json()
            }

            throw new Error ('Something wrong with network request')
        }).then(message => {
            console.log(message);
            this.setState({messages: message})
        })

    }

    Text = event => {
        this.setState({text: event.target.value})
    };

    Author = event => {
        this.setState({author: event.target.value})
    };

    addMessage = () => {

        const url = 'http://146.185.154.90:8000/messages';

        const data = new URLSearchParams();
        data.set('message', this.state.text);
        data.set('author', this.state.author);

        fetch(url, {
            method: 'post',
            body: data,
        }).then( post => {
            if (post.ok) {
                return post.json();
            }
        }).then(success => {

            let copyState = [...this.state.messages];

            copyState.push(success);

            this.setState({messages: copyState});
        })

    };


    render() {
        return (
            <div className="Mes">
                <SendMessage
                    addMessage={() => this.addMessage()}
                    onChangeText={(e) => this.Text(e)}
                    onChangeAuthor={(e) => this.Author(e)} />
                <GetMessage
                    message={this.state.messages} />
            </div>
        );
    }
}
export default Message;
