import React from 'react';
import './GetMessage.css';

const GetMessage = (props) => {
    return (
       props.message.map((message, index) => {
           return (
               <div key={index} className="message">
                   <p className="author">{message.author} :  </p>
                   <p className="msg">" {message.message} "</p>
                   <p className="datetime"> {message.datetime}</p>
               </div>
           )
       })
    );
};

export default GetMessage;